// Test d'exemple par défaut :
describe('The Traffic web site home page', () => {
    it('successfully loads', () => {
        cy.visit('/')
    })
})

describe('Scénario 1 : ...', () => {
    it('successfully loads', () => {
        cy.get('a[href="#configuration"]').click();
        cy.url().should('include', '/#configuration')
    });
});

describe('Scénario 2 : ...', () => {
    it('successfully loads', () => {
        cy.request('GET','http://127.0.0.1:4567/elements').as('elements');
        cy.get('@elements').should((response)=> {
            expect(response.body['segments']).to.have.length( 28)
        })
    });
});

describe('Scénario 3 : ...', () => {
    it('successfully loads', () => {
        cy.get('table').contains('td', 'No vehicle available');
    });
});

describe('Scénario 4 : ...', () => {
    it('successfully loads', () => {
        cy.get('input[name="speed"][form="segment-5"]').clear().type('30');
        cy.get('form[id="segment-5"]').within(() => {
            cy.get('button').click();
        })
        cy.get('.modal-dialog').within( () => {
            cy.get('.btn-pill').click();
        })
    });
});

describe('Scénario 5 : ...', () => {
    it('successfully loads', () => {
        cy.get('input[name="capacity"][form="roundabout-31"]').clear().type('4');
        cy.get('input[name="duration"][form="roundabout-31"]').clear().type('15');

        cy.get('form[name="roundabout-31"]').within(() => {
            cy.get('button').click();
        })

        cy.get('.modal-dialog').within( () => {
            cy.get('.btn-pill').click();
        })

        cy.reload();

        cy.request('GET','http://127.0.0.1:4567/elements').as('elements');
        cy.get('@elements').should((response)=> {
            expect(response.body['crossroads'][2]).to.have.property( 'capacity', 4);
            expect(response.body['crossroads'][2]).to.have.property( 'duration', 15);
        })

        cy.get('a[href="#configuration"]').click();
        cy.url().should('include', '/#configuration')

    });
});

describe('Scénario 6 : ...', () => {
    it('successfully loads', () => {
        cy.get('input[name="orangeDuration"][form="trafficlight-29"]').clear().type('4');
        cy.get('input[name="greenDuration"][form="trafficlight-29"]').clear().type('40');
        cy.get('input[name="nextPassageDuration"][form="trafficlight-29"]').clear().type('8');

        cy.get('form[name="trafficlight-29"]').within(() => {
            cy.get('button').click();
        })

        cy.get('.modal-dialog').within( () => {
            cy.get('.btn-pill').click();
        })

        cy.reload();

        cy.request('GET','http://127.0.0.1:4567/elements').as('elements');
        cy.get('@elements').should((response)=> {
            expect(response.body['crossroads'][0]).to.have.property( 'orangeDuration', 4);
            expect(response.body['crossroads'][0]).to.have.property( 'greenDuration', 40);
            expect(response.body['crossroads'][0]).to.have.property( 'nextPassageDuration', 8);
        })

        cy.get('a[href="#configuration"]').click();
        cy.url().should('include', '/#configuration')
    })
});

describe('Scénario 7 : ...', () => {
    it('successfully loads', () => {
        cy.get('input[name="origin"][form="addVehicle"]').clear().type('5');
        cy.get('input[name="destination"][form="addVehicle"]').clear().type('26');
        cy.get('input[name="time"][form="addVehicle"]').clear().type('50');

        cy.get('form[name="addVehicle"]').within(() => {
            cy.get('button').click();
        })

        cy.get('.modal-dialog').within( () => {
            cy.get('.btn-pill').click();
        })

        cy.get('input[name="origin"][form="addVehicle"]').clear().type('19');
        cy.get('input[name="destination"][form="addVehicle"]').clear().type('8');
        cy.get('input[name="time"][form="addVehicle"]').clear().type('200');

        cy.get('form[name="addVehicle"]').within(() => {
            cy.get('button').click();
        })

        cy.get('.modal-dialog').within( () => {
            cy.get('.btn-pill').click();
        })

        cy.get('input[name="origin"][form="addVehicle"]').clear().type('27');
        cy.get('input[name="destination"][form="addVehicle"]').clear().type('2');
        cy.get('input[name="time"][form="addVehicle"]').clear().type('150');

        cy.get('form[name="addVehicle"]').within(() => {
            cy.get('button').click();
        })

        cy.get('.modal-dialog').within( () => {
            cy.get('.btn-pill').click();
        })

        cy.request('GET','http://127.0.0.1:4567/vehicles').as('elements');
        cy.get('@elements').should((response)=> {
            expect(response.body['50.0'][0]).to.have.property( 'position', 0);
            expect(response.body['50.0'][0]).to.have.property( 'speed', 0);

            expect(response.body['200.0'][0]).to.have.property( 'position', 0);
            expect(response.body['200.0'][0]).to.have.property( 'speed', 0);

            expect(response.body['150.0'][0]).to.have.property( 'position', 0);
            expect(response.body['150.0'][0]).to.have.property( 'speed', 0);
        })

        cy.get('.col-md-8 .table tbody').find('tr').its('length').should('eq', 3)

    })
});

describe('Scénario 8 : ...', () => {
    it('successfully loads', () => {
        cy.get('a[href="#simulation"]').click();
        cy.url().should('include', '/#simulation')

        cy.get('input[name="time"][form="runNetwork"]').clear().type('120');

        cy.get('form[name="runNetwork"]').within(() => {
            cy.get('button').click();
        })

        cy.get('[aria-valuenow="100"]', { timeout: 120000 }).should('be.visible');

        cy.get('table tbody tr').eq(2).contains( 'span','play_circle_filled');

    })
});

describe('Scénario 9 : ...', () => {
    it('successfully loads', () => {
        cy.reload();

        cy.get('a[href="#configuration"]').click();
        cy.url().should('include', '/#configuration')

        cy.request('GET','http://127.0.0.1:4567/vehicles').as('elements');
        cy.get('@elements').should((response)=> {
            expect(response.body == [])
        })

        cy.get('input[name="origin"][form="addVehicle"]').clear().type('5');
        cy.get('input[name="destination"][form="addVehicle"]').clear().type('26');
        cy.get('input[name="time"][form="addVehicle"]').clear().type('50');

        cy.get('form[name="addVehicle"]').within(() => {
            cy.get('button').click();
        })

        cy.get('.modal-dialog').within( () => {
            cy.get('.btn-pill').click();
        })

        cy.get('input[name="origin"][form="addVehicle"]').clear().type('19');
        cy.get('input[name="destination"][form="addVehicle"]').clear().type('8');
        cy.get('input[name="time"][form="addVehicle"]').clear().type('200');

        cy.get('form[name="addVehicle"]').within(() => {
            cy.get('button').click();
        })

        cy.get('.modal-dialog').within( () => {
            cy.get('.btn-pill').click();
        })

        cy.get('input[name="origin"][form="addVehicle"]').clear().type('27');
        cy.get('input[name="destination"][form="addVehicle"]').clear().type('2');
        cy.get('input[name="time"][form="addVehicle"]').clear().type('150');

        cy.get('form[name="addVehicle"]').within(() => {
            cy.get('button').click();
        })

        cy.get('.modal-dialog').within( () => {
            cy.get('.btn-pill').click();
        })

        cy.get('a[href="#simulation"]').click();
        cy.url().should('include', '/#simulation')

        cy.get('input[name="time"][form="runNetwork"]').clear().type('500');

        cy.get('form[name="runNetwork"]').within(() => {
            cy.get('button').click();
        })

        cy.get('[aria-valuenow="100"]', { timeout: 500000 }).should('be.visible');


        cy.get('play_circle_filled').should('not.exist');

    })
});

describe('Scénario 10 : ...', () => {
    it('successfully loads', () => {
        cy.reload();

        cy.get('a[href="#configuration"]').click();
        cy.url().should('include', '/#configuration')

        cy.request('GET','http://127.0.0.1:4567/vehicles').as('elements');
        cy.get('@elements').should((response)=> {
            expect(response.body == [])
        })

        cy.get('input[name="origin"][form="addVehicle"]').clear().type('5');
        cy.get('input[name="destination"][form="addVehicle"]').clear().type('26');
        cy.get('input[name="time"][form="addVehicle"]').clear().type('50');

        cy.get('form[name="addVehicle"]').within(() => {
            cy.get('button').click();
        })

        cy.get('.modal-dialog').within( () => {
            cy.get('.btn-pill').click();
        })

        cy.get('input[name="origin"][form="addVehicle"]').clear().type('5');
        cy.get('input[name="destination"][form="addVehicle"]').clear().type('26');
        cy.get('input[name="time"][form="addVehicle"]').clear().type('80');

        cy.get('form[name="addVehicle"]').within(() => {
            cy.get('button').click();
        })

        cy.get('.modal-dialog').within( () => {
            cy.get('.btn-pill').click();
        })

        cy.get('input[name="origin"][form="addVehicle"]').clear().type('5');
        cy.get('input[name="destination"][form="addVehicle"]').clear().type('26');
        cy.get('input[name="time"][form="addVehicle"]').clear().type('80');

        cy.get('form[name="addVehicle"]').within(() => {
            cy.get('button').click();
        })

        cy.get('.modal-dialog').within( () => {
            cy.get('.btn-pill').click();
        })

        cy.get('a[href="#simulation"]').click();
        cy.url().should('include', '/#simulation')

        cy.get('input[name="time"][form="runNetwork"]').clear().type('200');

        cy.get('form[name="runNetwork"]').within(() => {
            cy.get('button').click();
        })

        cy.get('[aria-valuenow="100"]', { timeout: 200000 }).should('be.visible');


        cy.get('table tbody tr').eq(0).contains( '29');
        cy.get('table tbody tr').eq(1).contains( '29');
        cy.get('table tbody tr').eq(2).contains( '17');

    })
});
